# JOS LAB UNIMI
This repository is a copy of [6.828 class](https://pdos.csail.mit.edu/6.828/2014/) JOS repository.
It is configured using Gitlab-CI to automate the assessment of laboratories.

## LAB GRADE STAUS

### Lab 1
 [![build status](https://gitlab.com/_Ricky_/JOS-Lab/badges/lab1/build.svg)](https://gitlab.com/_Ricky_/JOS-Lab/commits/lab1)

### Lab 2
 [![build status](https://gitlab.com/_Ricky_/JOS-Lab/badges/lab2/build.svg)](https://gitlab.com/_Ricky_/JOS-Lab/commits/lab2)

### Lab 3
 [![build status](https://gitlab.com/_Ricky_/JOS-Lab/badges/lab3/build.svg)](https://gitlab.com/_Ricky_/JOS-Lab/commits/lab3)
