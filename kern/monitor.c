// Simple command-line kernel monitor useful for
// controlling the kernel and exploring the system interactively.

#include <inc/stdio.h>
#include <inc/string.h>
#include <inc/memlayout.h>
#include <inc/mmu.h>
#include <inc/assert.h>
#include <inc/x86.h>

#include <kern/console.h>
#include <kern/monitor.h>
#include <kern/kdebug.h>
#include <kern/trap.h>
#include <kern/pmap.h>

#define CMDBUF_SIZE	80	// enough for one VGA text line

struct Command {
	const char *name;
	const char *desc;
	// return -1 to force monitor to exit
	int (*func)(int argc, char** argv, struct Trapframe* tf);
};

static struct Command commands[] = {
	{ "help", "Display this list of commands", mon_help },
	{ "kerninfo", "Display information about the kernel", mon_kerninfo },
	{ "backtrace", "Display current calling stack", mon_backtrace},
	{ "translate", "Translate a virtual address into its physical counterpart", mon_translate },
	{ "pages", "Display physical/virtual address of pages 1-5", mon_pages },
};
#define NCOMMANDS (sizeof(commands)/sizeof(commands[0]))

/***** Implementations of basic kernel monitor commands *****/

int
mon_help(int argc, char **argv, struct Trapframe *tf)
{
	int i;

	for (i = 0; i < NCOMMANDS; i++)
		cprintf("%s - %s\n", commands[i].name, commands[i].desc);
	return 0;
}

int
mon_kerninfo(int argc, char **argv, struct Trapframe *tf)
{
	extern char _start[], entry[], etext[], edata[], end[];

	cprintf("Special kernel symbols:\n");
	cprintf("  _start                  %08x (phys)\n", _start);
	cprintf("  entry  %08x (virt)  %08x (phys)\n", entry, entry - KERNBASE);
	cprintf("  etext  %08x (virt)  %08x (phys)\n", etext, etext - KERNBASE);
	cprintf("  edata  %08x (virt)  %08x (phys)\n", edata, edata - KERNBASE);
	cprintf("  end    %08x (virt)  %08x (phys)\n", end, end - KERNBASE);
	cprintf("Kernel executable memory footprint: %dKB\n",
		ROUNDUP(end - entry, 1024) / 1024);
	return 0;
}

#define EBP(_addr) ((uint32_t)_addr)
#define EIP(_ebp) ((uint32_t) *(_ebp + 1))
#define ARG(_ebp, _num) ((uint32_t) *(_ebp + 2 + _num))

int
mon_backtrace(int argc, char **argv, struct Trapframe *tf)
{
	uint32_t *addr = 0;
	struct Eipdebuginfo debug;

	cprintf("Stack backtrace:\n");

	for(addr = (uint32_t *) read_ebp(); addr != NULL; addr = (uint32_t *) *addr)
	{
		cprintf ("  ebp %08x  eip %08x  args %08x %08x %08x %08x\n", EBP (addr),
			EIP (addr), ARG (addr, 0), ARG (addr, 1), ARG (addr, 2), ARG (addr, 3), ARG (addr, 4));

			debuginfo_eip (EIP (addr), &debug);

			cprintf ("       %s:%d: %.*s+%d\n",
				debug.eip_file,
				debug.eip_line,
				debug.eip_fn_namelen,
				debug.eip_fn_name,
				EIP (addr) - debug.eip_fn_addr);
	}
	return 0;
}

int
mon_translate(int argc, char **argv, struct Trapframe *tf)
{
	pte_t *pte = NULL;
	uint32_t offset = 0;
	physaddr_t address = 0;
	uint32_t virtual = 0;

	if (argc != 2) {
		cprintf("No arguments or too many arguments, retry with 'translate $va'.\n");
		return 0;
	}

	virtual = (uint32_t)strtol(argv[1], NULL, 16);
	offset = PGOFF(virtual);
	pte = pgdir_walk(kern_pgdir, (void *)virtual, false);

	if (pte && (*pte & PTE_P)) {
		address = PTE_ADDR(*pte);
		address = address | offset;
		cprintf("Va 0x%x -> Pa 0x%x\n", virtual, address);
		return 0;
	}
	cprintf("No mapping!\n");
	return 0;
}

int mon_pages(int argc, char **argv, struct Trapframe *tf)
{
	size_t i = 0;
	physaddr_t addr = 0;
	uintptr_t virt = 0;
	struct PageInfo *pointer = NULL;
	for(; i < 5; i++)
	{
		pointer = pages + i;
		addr = page2pa(pointer);
		virt = (uintptr_t)page2kva(pointer);
		cprintf("Page %u - P: 0x%x V: 0x%x\n", i, addr, virt);
	}
	return 0;
}


/***** Kernel monitor command interpreter *****/

#define WHITESPACE "\t\r\n "
#define MAXARGS 16

static int
runcmd(char *buf, struct Trapframe *tf)
{
	int argc;
	char *argv[MAXARGS];
	int i;

	// Parse the command buffer into whitespace-separated arguments
	argc = 0;
	argv[argc] = 0;
	while (1) {
		// gobble whitespace
		while (*buf && strchr(WHITESPACE, *buf))
			*buf++ = 0;
		if (*buf == 0)
			break;

		// save and scan past next arg
		if (argc == MAXARGS-1) {
			cprintf("Too many arguments (max %d)\n", MAXARGS);
			return 0;
		}
		argv[argc++] = buf;
		while (*buf && !strchr(WHITESPACE, *buf))
			buf++;
	}
	argv[argc] = 0;

	// Lookup and invoke the command
	if (argc == 0)
		return 0;
	for (i = 0; i < NCOMMANDS; i++) {
		if (strcmp(argv[0], commands[i].name) == 0)
			return commands[i].func(argc, argv, tf);
	}
	cprintf("Unknown command '%s'\n", argv[0]);
	return 0;
}

void
monitor(struct Trapframe *tf)
{
	char *buf;

	cprintf("Welcome to the JOS kernel monitor!\n");
	cprintf("Type 'help' for a list of commands.\n");

	if (tf != NULL)
		print_trapframe(tf);

	while (1) {
		buf = readline("K> ");
		if (buf != NULL)
			if (runcmd(buf, tf) < 0)
				break;
	}
}
